#!/bin/bash

# Generate list of Julia packages
JULIA_PKGS=$(julia -e "using Pkg; for p in Pkg.installed() println(p.first) end;")

# Then instantiate each one in order to ensure they're unpacked at image build time
for pkg in $JULIA_PKGS; do
	echo -n "Preloading julia package: $pkg ... "
	julia -e "using $pkg"
	echo "done"
done
