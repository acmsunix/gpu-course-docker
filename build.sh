#!/bin/bash

VERCACHE=.vercache
BUILD_DATE=`date +%Y%m%d`
#BUILDOPTS="--no-cache"

[ -d $VERCACHE ] || mkdir -p $VERCACHE || exit 1

if (( $# > 0 )); then
	TARGETS="$@"
else
	TARGETS="ets-pytorch-py2 ets-pytorch-py3"
fi

for target in $TARGETS; do
	SEQUENCE=0
	while true; do
		SEQUENCE=$(( $SEQUENCE + 1))
		ver=${target}-${BUILD_DATE}v${SEQUENCE}
		tag=ucsdets/instructional:${ver}
		cleantag=$( echo "$tag" | sed -e 's#[^0-9a-zA-Z]#_#g')
		[ -f ${VERCACHE}/${cleantag} ] || break
	done
	touch ${VERCACHE}/${cleantag}
		
	latest_tag=ucsdets/instructional:${target}-latest
	( cd $target; docker build ${BUILDOPTS} -t $tag .; docker push $tag)

	echo "Fetching package versions into PKG/${ver}.*"
	docker run ${tag} conda list -e > PKG/${ver}.conda
	docker run ${tag} pip freeze > PKG/${ver}.pip
	docker run ${tag} dpkg -l > PKG/${ver}.dpkg

	echo "# Promote this to -latest via:"
	echo "docker tag $tag $latest_tag ; docker push $latest_tag"

done

