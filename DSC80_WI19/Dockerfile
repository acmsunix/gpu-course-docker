FROM jupyter/datascience-notebook

MAINTAINER UC San Diego ITS/ETS-EdTech-Ecosystems <acms-compinf@ucsd.edu>

# jhub must match hub version (https://zero-to-jupyterhub-with-kubernetes.readthedocs.io/en/latest/user-environment.html?highlight=docker%20hub)
# below is what's done in sample (https://github.com/jupyterhub/zero-to-jupyterhub-k8s/blob/master/images/singleuser-sample/Dockerfile
## pin jupyterhub to match the Hub version
## set via --build-arg in Makefile
#ARG JUPYTERHUB_VERSION=0.9.*
#RUN pip install --no-cache jupyterhub==$JUPYTERHUB_VERSION

USER root

RUN pip install datascience

# basic linux commands
RUN apt-get update && apt-get -qq install -y \
        curl \
        rsync \
        unzip \
        less nano vim \
        openssh-client \
        wget

# DSC80_WI19: no okpy
#RUN pip install okpy
# DSC80_WI19: folium; in datascience-notebook
#RUN pip install folium

# DSC10_?18: python3 notebooks have matplotlib 2.1 (due to gdbm below?), janine needs 2.0.2, revert below
#RUN conda install matplotlib==2.0.0
RUN conda install -y numpy==1.12.1
RUN conda install -y pandas==0.19.2
RUN conda install -y pypandoc
RUN conda install -y scipy==0.19.0
RUN conda install -y statsmodels==0.8.0

# The following are cherry-picked from the DS8/datahub container specs

########################
# Pruned from user-containers/datahub/install-python-packages.bash
# (and removed version pinning for now)
# pj: add nose per unpingo ECE180
RUN conda install --quiet --yes \
            bokeh \
            cloudpickle \
            cython \
            dill \
            h5py \
            hdf5 \
            nose \
            numba \
            numexpr \
            patsy \
            scikit-image \
            scikit-learn \
            seaborn \
            sqlalchemy \
            sympy

# DSC80_WI19: marina wants jupyterlab; installed by default; more info: see https://jupyterlab.readthedocs.io/en/stable/user/jupyterhub.html
#RUN conda install --quiet --yes jupyterlab

# Remove pyqt and qt, since we do not need them with notebooks
RUN conda remove --quiet --yes --force qt pyqt
RUN conda clean -tipsy


########################
# Pruned from user-containers/datahub/install-extensions.bash

# nbgrader
RUN conda install nbgrader
RUN jupyter nbextension install --symlink --sys-prefix --py nbgrader
RUN jupyter nbextension enable --sys-prefix --py nbgrader
RUN jupyter serverextension enable --sys-prefix --py nbgrader

# disable formgrader, create-assignments for all.  grader and assignment maker will run below with 'enable --user' instead of 'disable --sys-prefix'
RUN jupyter nbextension disable --sys-prefix formgrader/main --section=tree
RUN jupyter serverextension disable --sys-prefix nbgrader.server_extensions.formgrader
RUN jupyter nbextension disable --sys-prefix create_assignment/main

# ipywidgets!
RUN pip install ipywidgets
RUN jupyter nbextension enable --sys-prefix --py widgetsnbextension

# replace nbpuller with nbgitpuller
RUN pip install git+https://github.com/data-8/gitautosync
RUN jupyter serverextension enable --py nbgitpuller --sys-prefix

# nbresuse to show users memory usage
RUN pip install git+https://github.com/data-8/nbresuse.git
RUN jupyter serverextension enable --sys-prefix --py nbresuse
RUN jupyter nbextension install --sys-prefix --py nbresuse
RUN jupyter nbextension enable --sys-prefix --py nbresuse


########################
# Pulled from dockerspawner/systemuser/Dockerfile

WORKDIR /home
RUN userdel jovyan && rm -rf /home/jovyan
ENV SHELL /bin/bash

ADD systemuser.sh /usr/local/bin/start-systemuser.sh

# smoke test entrypoint
RUN USER_ID=65000 USER=systemusertest sh /usr/local/bin/start-systemuser.sh -h && userdel systemusertest

# Random fixups at the end
# Julia evidently ships a few precompiled .ji files with file mode 0600, not terribly useful in our environment
RUN bash -c 'find /opt/julia -type f -a -name "*.ji" -a \! -perm /005 | xargs chmod og+rX'

CMD ["sh", "/usr/local/bin/start-systemuser.sh"]

# install gdbm per adam
RUN apt-get update
RUN apt-get -y install python3-gdbm

# DSC10_?18: revert matplotlib to 2.0.2 per janine
# DSC80_WI19 build: conflicts; remove for now
# RUN conda install matplotlib==2.0.2
RUN conda list --revisions

# Pre-generate font cache so the user does not see fc-list warning when
# importing datascience. https://github.com/matplotlib/matplotlib/issues/5836
RUN python -c 'import matplotlib.pyplot'

